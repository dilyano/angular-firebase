export const environment = {
  firebase: {
    projectId: '$firebase_projectid',
    appId: '$firebase_appid',
    storageBucket: '$firebase_storagebucket',
    apiKey: '$firebase_apikey',
    authDomain: '$firebase_authdomain',
    messagingSenderId: '$firebase_messagingsenderid',
    measurementId: '$firebase_measurementid',
  },
  production: true
};
